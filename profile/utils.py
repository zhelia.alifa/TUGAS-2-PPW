from .models import DataProfil

def get_data_user(request, tipe):
	data = None
	if tipe == "user_login" and 'user_login' in request.session:
		data = request.session['user_login']
	elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
		data = request.session['kode_identitas']
	return data

def create_new_user(request):
	npm = get_data_user(request, 'kode_identitas')
	profile = DataProfil()
	profile.npm = npm
	profile.save()
	return profile

def check_user_in_database(request, npm):
	is_exist = False
	npm = get_data_user(request, 'kode_identitas')
	count_profile = DataProfil.objects.filter(npm=npm).count()
	if count_profile > 0:
		is_exist = True

	return is_exist