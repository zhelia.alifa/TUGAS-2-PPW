from django.db import models

# Create your models here.

class DataProfil(models.Model):
	name = models.CharField(max_length=250)
	npm = models.CharField(max_length=20)
	keahlian = models.CharField(max_length=200)
	email = models.EmailField(max_length=200)
	profilLinkedin = models.URLField(max_length=200)