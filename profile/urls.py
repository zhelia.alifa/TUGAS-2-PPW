from django.conf.urls import url
from .views import index, edit

#url for app, add your URL Configuration

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^edit/$', edit, name='edit'),
    ]