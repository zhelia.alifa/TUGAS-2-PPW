from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from django.urls import resolve
from django.contrib.auth.models import *
from .views import index
import datetime
from .models import Profile       
    
def test_profile_url_is_exist(self) #pragma: no cover:
    profile = Profile(name="abc",birthday=datetime.datetime.now(),gender="male",expert="Coding,playing",description="hohoh",email="abc@abc.com")
    profile.save()
    response = Client().get('/profile/')
    self.assertEqual(response.status_code, 200)
    profile.delete()
    self.assertEqual(Profile.objects.all().count(),0)

def test_profile_using_index_func(self) #pragma: no cover:
    found = resolve('/profile/')
    self.assertEqual(found.func, index)