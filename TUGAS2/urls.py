"""TUGAS2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
import profile.urls as profile
import login_status.urls as login_status
import halaman_status.urls as halaman_status
import riwayat.urls as riwayat
import pencarian_mahasiswa.urls as pencarian_mahasiswa
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profile/', include(profile,namespace='profile')), 
    url(r'^admin/', admin.site.urls),
    url(r'^login-status/', include(login_status, namespace='login-status')),
    url(r'^halaman-status/', include(halaman_status, namespace='halaman-status')),
    url(r'^riwayat/', include(riwayat, namespace='riwayat')),
    url(r'^pencarian-mahasiswa/', include(pencarian_mahasiswa, namespace='pencarian-mahasiswa')),

]
