from django.conf.urls import url
from .views import *

from login_status.custom_auth import auth_login

urlpatterns = [
	url(r'^$', masuk_login, name='masuk_login'),
	url(r'^index/$', index, name='index'),

	#custom_auth
	url(r'^custom_auth/login/$', auth_login, name='auth_login'),

]