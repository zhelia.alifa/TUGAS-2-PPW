from django.test import TestCase
from django.test import Client
from django.urls import resolve

# Create your tests here.
class LoginStatusUnitTest(TestCase):
	def test_login_status_url_is_exist(self):
		response = Client().get('/login-status/')
		self.assertEqual(response.status_code, 200)