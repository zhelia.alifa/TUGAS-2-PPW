from __future__ import unicode_literals
from django.shortcuts import render

from django.http import HttpResponseRedirect
from django.urls import reverse
import requests
from .api_riwayat import get_riwayat

response = {}

def index(request):
	response['riwayat'] = get_riwayat().json()
	html = 'riwayat.html'
	response['rahasia'] = False
	return render(request, html, response)
