from django.db import models

class Pengguna(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True,)
    nama = models.CharField('Nama', max_length=200)
    npm = models.CharField('NPM',max_length = 12)
    email = models.CharField('email',max_length = 50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class MessageKu(models.Model):
    pengguna = models.ForeignKey(Pengguna,on_delete=models.CASCADE)
    message = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)