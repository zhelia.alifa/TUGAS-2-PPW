from django.conf.urls import url
from .views import index, message_post, hal_status, get_data_session, get_data_user, create_new_user, set_data_for_session

urlpatterns = [
    url(r'^(?P<username>.*)/$', index, name='index'),
    url(r'^message_post,', message_post, name='message_post,'),
    url(r'^hal_status', hal_status, name='hal_status'),

 ]
