from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Pengguna, MessageKu
from login_status.views import index
from django.contrib import messages
from django.urls import reverse
response = {}
def index(request,username):
    if 'user_login' in request.session.keys():
        response['login'] = True
        return HttpResponseRedirect(reverse('halaman-status:hal_status'))
    else:
        response['login'] = False
        return HttpResponseRedirect(reverse('login-status:index'))
def message_post(request):
    form = Message_Form(request.POST or None)
    if((request.method == 'POST') and form.is_valid()):
        response['message'] = request.POST['message']
        username = request.session['user_login']
        pengguna = Pengguna.objects.get(nama = username)
        status = MessageKu(pengguna = pengguna, message=response['message'])
        #status.pengguna = pengguna
        status.save()

        message = MessageKu.objects.all()
        response['message'] = message
    return HttpResponseRedirect(reverse('halaman-status:hal_status'))

def hal_status(request):
    user = request.session['user_login']
    html = 'halaman_status/halaman_status.html'
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('login-status:index'))
    set_data_for_session(request)
    try:
        pengguna = Pengguna.objects.get(nama = user)
    except Exception as e:
        pengguna = create_new_user(request)
    statusnya = MessageKu.objects.filter(pengguna = pengguna).order_by('-id')
    response['status'] = statusnya
    response['status_form'] = Message_Form
    return render(request, html, response)


def get_data_session(request):
    if get_data_user(request, 'user_login'):
        response['author'] = get_data_user(request, 'user_login')

def set_data_for_session(request):
    response['author'] = get_data_user(request, 'user_login')
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data

def create_new_user(request):
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Pengguna()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.save()

    return pengguna