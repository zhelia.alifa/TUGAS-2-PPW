from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .api_csui_helper.csui_helper import CSUIhelper

from random import randint, sample
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

	mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
	auth = csui_helper.instance.get_auth_param_dict()

	response = {"mahasiswa_list": mahasiswa_list}
	html = 'pencarian_mahasiswa/pencarian_mahasiswa.html'
	page = request.GET.get('page', 1)
	paginator = Paginator(mahasiswa_list, 20)

	try:
	    users = paginator.page(page)
	except PageNotAnInteger:
	    users = paginator.page(1)
	except EmptyPage:
	    users = paginator.page(paginator.num_pages)

	expertises = ['Java', 'C#', 'C++', 'Pascal']
	level = ['BEGINNER','INTERMEDIATE', 'ADVANCED']

	for user in users:
		count = randint(1, 3)
		user_expertises = sample(expertises, count)
		user_levels = [level[randint(0, len(level)-1)] for i in range(count)]
		user['expertises'] = zip(user_expertises, user_levels)

	response = {"mahasiswa_list": users}
	html = 'pencarian_mahasiswa/pencarian_mahasiswa.html'
	return render(request, html, response)