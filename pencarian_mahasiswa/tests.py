from django.test import Client
from django.test import TestCase
from django.urls import resolve
from .views import index
# Create your tests here.
class PencarianMahasiswaTest(TestCase):
	def test_pencarian_mahasiswa_url_is_exist(self):
		response = Client().get('/pencarian-mahasiswa/')
		self.assertEqual(response.status_code, 200)

	def test_pencarian_mahasiswa_using_index_func(self):
		found = resolve('/pencarian-mahasiswa/')
		self.assertEqual(found.func, index)