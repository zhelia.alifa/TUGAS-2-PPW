from django.apps import AppConfig


class PencarianMahasiswaConfig(AppConfig):
    name = 'pencarian_mahasiswa'
